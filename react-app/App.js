import React from 'react';

import { StyleSheet, View, Image, Text, TextInput } from 'react-native';

export default function App() {
  return (
    <View style={styles.container}>
      <Image
        style={styles.logo}
        source={require('./img/logo.png')}
      />
      <Text style={styles.text}>Log in</Text>
      <TextInput
        style={styles.inputLogin}
        defaultValue="Email"
      />
      <TextInput
        style={styles.inputLogin}
        defaultValue="Password"
      />
      <Text style={styles.textSecondary}>Forgot password</Text>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
  },
  logo: {
    paddingTop: 25,
    width: 250,
    height: 150,
  },
  text: {
    color: "#009EFD",
    marginBottom: 15,
    fontSize: 28,
  },
  inputLogin: {
    width: 300,
    height: 40,
    borderColor: 'gray',
    borderWidth: 1,
    borderRadius: 5,
    marginBottom: 15,
    padding: 8,
  },
  textSecondary: {
    fontSize: 12,
  }
});
